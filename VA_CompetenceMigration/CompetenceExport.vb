﻿Imports VA_CompetenceMigrationService
Imports System.Text
Imports System.Configuration
Imports System.IO
Imports System.Net.Mail

Module CompetenceExport

    Private Const strTIMESTAMP_FORMAT As String = "yyyy-MM-dd HH:mm:ss.fff"

    Sub Main(ByVal args As String())

        ' Check that Id is given
        If args.Length > 0 Then
            Dim strCustomerId As String = args(0)

            ' Read config file
            Dim intLogLevel As Integer = ConfigurationManager.AppSettings("LogLevel")
            Dim strLogFilename As String = ConfigurationManager.AppSettings("LogFilename")
            Dim strSmtpServer As String = ConfigurationManager.AppSettings("SmtpServer")
            Dim strMailSender As String = ConfigurationManager.AppSettings("MailSender")
            Dim strMailRecipient As String = ConfigurationManager.AppSettings("MailRecipient")

            ' Add timestamp when starting
            Dim strResult As New StringBuilder()
            If intLogLevel > 1 Then
                strResult.AppendLine(String.Format("{0} Executing export: {1}", Date.Now.ToString(strTIMESTAMP_FORMAT), strCustomerId) & vbCrLf)
            End If

            Try
                ' Create instance of export service
                Dim objService As New CompetenceExportService()
                objService.CustomerId = strCustomerId

                ' Execute method
                Select Case objService.ExecuteService()
                    Case 1
                        ' Manage informational messages
                        If intLogLevel > 1 Then
                            strResult.AppendLine("Information:")
                            strResult.AppendLine(objService.GetInformation)
                        End If
                    Case 2
                        If intLogLevel > 1 Then
                            ' Manage warnings
                            strResult.AppendLine("Warning:")
                            strResult.AppendLine(objService.GetWarnings)
                        End If
                    Case 3
                        If intLogLevel > 1 Then
                            ' Manage informational messages AND warnings
                            strResult.AppendLine("Warning:")
                            strResult.AppendLine(objService.GetWarnings)
                            strResult.AppendLine("Information:")
                            strResult.AppendLine(objService.GetInformation)
                        End If
                    Case 4
                        If intLogLevel > 0 Then
                            ' Manage exceptions
                            strResult.AppendLine("Exception:")
                            strResult.AppendLine(objService.GetExceptions)
                        End If
                    Case 5
                        If intLogLevel > 0 Then
                            ' Manage informational messages AND exceptions
                            strResult.AppendLine("Exception:")
                            strResult.AppendLine(objService.GetExceptions)
                        End If
                        If intLogLevel > 1 Then
                            strResult.AppendLine("Information:")
                            strResult.AppendLine(objService.GetInformation)
                        End If
                    Case 6
                        If intLogLevel > 0 Then
                            ' Manage warnings AND exceptions
                            strResult.AppendLine("Exception:")
                            strResult.AppendLine(objService.GetExceptions)
                        End If
                        If intLogLevel > 1 Then
                            strResult.AppendLine("Warning:")
                            strResult.AppendLine(objService.GetWarnings)
                        End If
                    Case 7
                        If intLogLevel > 0 Then
                            ' Manage informational messages AND warnings AND exceptions
                            strResult.AppendLine("Exception:")
                            strResult.AppendLine(objService.GetExceptions)
                        End If
                        If intLogLevel > 1 Then
                            strResult.AppendLine("Warning:")
                            strResult.AppendLine(objService.GetWarnings)
                            strResult.AppendLine("Information:")
                            strResult.AppendLine(objService.GetInformation)
                        End If
                    Case Else
                        ' Success - do nothing
                End Select

                ' Check if output file is given
                Dim strOutputFile As String = objService.GetOutputFile()
                If intLogLevel > 1 Then
                    If strOutputFile.Length > 0 Then
                        strResult.AppendLine(String.Format("Output file '{0}'", strOutputFile))
                    End If
                End If

                ' Add timestamp when finishing
                If intLogLevel > 1 Then
                    strResult.AppendLine()
                    strResult.AppendLine(String.Format("{0} Done!", Date.Now.ToString(strTIMESTAMP_FORMAT)))
                End If

            Catch ex As Exception
                If intLogLevel > 0 Then
                    strResult.AppendLine("Exception:")
                    strResult.AppendLine(ex.ToString())
                End If

                If intLogLevel > 1 Then
                    strResult.AppendLine()
                    strResult.AppendLine(String.Format("{0} Failed!", Date.Now.ToString(strTIMESTAMP_FORMAT)))
                End If

            Finally
                ' Check result
                If strResult.Length > 0 Then
                    Dim blnWritten As Boolean
                    If strSmtpServer.Length > 0 Then
                        SendMailMessage(strSmtpServer, strMailSender, strMailRecipient, strResult)
                        blnWritten = True
                    End If
                    If strLogFilename.Length > 0 Then
                        WriteToLogFile(strLogFilename, strResult)
                        blnWritten = True
                    End If
                    If Not blnWritten Then
                        Console.WriteLine(strResult.ToString())
                    End If
                End If
            End Try
        Else
            Console.WriteLine("Customer Id must be given as an argument")
        End If
    End Sub

    Private Sub WriteToLogFile(ByVal p_strLogFile As String, ByVal p_strMessage As StringBuilder)
        Using outfile As New StreamWriter(p_strLogFile, False, Encoding.UTF8)
            outfile.Write(p_strMessage.ToString())
        End Using
    End Sub

    Private Sub SendMailMessage(ByVal p_strSmtpServer As String, ByVal p_strMailSender As String, ByVal p_strMailRecipient As String, ByVal p_strMessage As StringBuilder)
        Try
            Dim objMailMsg As New MailMessage(p_strMailSender, p_strMailRecipient)
            objMailMsg.Subject = "E-post från VA_PersonExport"
            objMailMsg.Body = p_strMessage.ToString
            objMailMsg.Priority = MailPriority.Normal
            objMailMsg.IsBodyHtml = False

            Dim objSmtpMail As New SmtpClient
            With objSmtpMail
                .Host = p_strSmtpServer
                .UseDefaultCredentials = True
            End With
            objSmtpMail.Send(objMailMsg)
        Catch ex As Exception
            p_strMessage.AppendLine("Mail Exception:")
            p_strMessage.AppendLine(ex.ToString())
        End Try
    End Sub

End Module
