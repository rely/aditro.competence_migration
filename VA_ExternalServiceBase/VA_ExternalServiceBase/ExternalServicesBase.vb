﻿Imports System.Runtime.InteropServices
Imports System.Text
Imports System.Xml
Imports System.IO

<GuidAttribute("CC64E3C8-1811-4b46-A1D3-C64F5E897BAB"), ClassInterface(ClassInterfaceType.AutoDual)> _
Public Class ExternalServicesBase
    Implements IExternalServicesBase
    Implements IDisposable

    Private m_strDocSettingsPath As String = Me.GetType.Assembly.Location.Replace(".dll", ".xml")
    Private m_docSettings As XmlDocument
    Private m_lstExceptions As New List(Of String)
    Private m_lstWarnings As New List(Of String)
    Private m_lstInformation As New List(Of String)
    Private m_strCustomerId As String = ""
    Private m_strInputFile As String = ""
    Private m_strOutputFile As String = ""

    Private disposedValue As Boolean = False        ' To detect redundant calls


    ' This function have to be overridden by child since this is where the actual implementation should be
    Public Overridable Function ExecuteService() As ResultLevel Implements IExternalServicesBase.ExecuteService
        ' Creates an exception if its not overridden
        Me.AddException(New NotImplementedException("ExecuteService method must be implemented by child"))
        Return ResultLevel.Exception
    End Function

    Public Property CustomerId() As String Implements IExternalServicesBase.CustomerId
        Get
            Return m_strCustomerId
        End Get
        Set(ByVal value As String)
            m_strCustomerId = value
        End Set
    End Property

    ' This function makes it possible to send a file to the service
    Public Sub SetInputFile(ByVal p_strFileName As String) Implements IExternalServicesBase.SetInputFile
        m_strInputFile = p_strFileName
    End Sub

    ' This function makes it possible to retrieve a file from the service
    Public Function GetOutputFile() As String Implements IExternalServicesBase.GetOutputFile
        Return m_strOutputFile
    End Function

    ' This function returns a string containing provided exceptions
    Public Function GetExceptions() As String Implements IExternalServicesBase.GetExceptions
        ' If no exceptions are added - return Nothing
        If m_lstExceptions.Count = 0 Then
            Return String.Empty
        Else
            ' Create a string that contains added messages
            Dim strMessages As New StringBuilder
            Dim intIndex As Integer = 0
            For Each message As String In m_lstExceptions
                strMessages.AppendLine(message)
                strMessages.AppendLine(String.Empty)
            Next
            Return strMessages.ToString()
        End If
    End Function

    ' This function returns a string containing provided warnings
    Public Function GetWarnings() As String Implements IExternalServicesBase.GetWarnings
        ' If no exceptions are added - return Nothing
        If m_lstWarnings.Count = 0 Then
            Return String.Empty
        Else
            ' Create a string that contains added messages
            Dim strMessages As New StringBuilder
            Dim intIndex As Integer = 0
            For Each message As String In m_lstWarnings
                strMessages.AppendLine(message)
                strMessages.AppendLine(String.Empty)
            Next
            Return strMessages.ToString()
        End If
    End Function

    ' This function returns a string containing provided information
    Public Function GetInformation() As String Implements IExternalServicesBase.GetInformation
        ' If no exceptions are added - return Nothing
        If m_lstInformation.Count = 0 Then
            Return String.Empty
        Else
            ' Create a string that contains added messages
            Dim strMessages As New StringBuilder
            Dim intIndex As Integer = 0
            For Each message As String In m_lstInformation
                strMessages.AppendLine(message)
                strMessages.AppendLine(String.Empty)
            Next
            Return strMessages.ToString()
        End If
    End Function


    ' This function returns the selected setting for the current customer id (or Nothing if document/setting is missing)
    Protected Function GetSetting(ByVal p_strKey As String) As String
        ' Find selected setting
        Return Me.GetSetting(Me.CustomerId, p_strKey)
    End Function

    ' This function returns the selected setting for the specific customer id (or Nothing if document/setting is missing)
    Protected Function GetSetting(ByVal p_strCustomerId As String, ByVal p_strKey As String) As String
        Try
            ' Try to load settings document
            If m_docSettings Is Nothing Then Me.LoadSettings()

            Dim objCustomer As XmlNode = m_docSettings.SelectSingleNode("Settings/CustomerSettings[@CustomerId='" & p_strCustomerId & "']")
            Dim objCommon As XmlNode = m_docSettings.SelectSingleNode("Settings/CommonSettings")
            Dim objSetting As XmlNode

            ' Check customer settings
            If objCustomer IsNot Nothing Then
                objSetting = objCustomer.SelectSingleNode("Setting[@key='" & p_strKey & "']")
                ' Setting is missing?
                If objSetting Is Nothing AndAlso objCommon IsNot Nothing Then
                    ' Check common settings
                    objSetting = objCommon.SelectSingleNode("Setting[@key='" & p_strKey & "']")
                End If
            ElseIf objCommon IsNot Nothing Then
                ' Check common settings
                objSetting = objCommon.SelectSingleNode("Setting[@key='" & p_strKey & "']")
            Else
                Return Nothing
            End If

            ' Setting found?
            If objSetting IsNot Nothing Then
                If objSetting.Attributes("encrypted") IsNot Nothing _
                AndAlso objSetting.Attributes("encrypted").Value.ToLower = "true" Then
                    Return Me.Decrypt(p_strKey, objSetting.InnerText)
                Else
                    Return objSetting.InnerText
                End If
            Else
                Return Nothing
            End If

        Catch ex As Exception
            Me.AddException(ex)
            Return Nothing
        End Try
    End Function

    ' This function returns multiple instances of the selected setting for the current customer id (or Nothing if document/setting is missing)
    Protected Function GetSettingWithMultipleValues(ByVal p_strKey As String) As List(Of String)
        Return Me.GetSettingWithMultipleValues(Me.CustomerId, p_strKey)
    End Function

    ' This function returns multiple instances of the selected setting for the specific customer id  (or Nothing if document/setting is missing)
    Protected Function GetSettingWithMultipleValues(ByVal p_strCustomerId As String, ByVal p_strKey As String) As List(Of String)
        Try
            ' Try to load settings document
            If m_docSettings Is Nothing Then Me.LoadSettings()

            Dim objCustomer As XmlNode = m_docSettings.SelectSingleNode("Settings/CustomerSettings[@CustomerId='" & p_strCustomerId & "']")
            Dim objCommon As XmlNode = m_docSettings.SelectSingleNode("Settings/CommonSettings")
            Dim objSettings As XmlNodeList

            ' Check customer settings
            If objCustomer IsNot Nothing Then
                objSettings = objCustomer.SelectNodes("Setting[@key='" & p_strKey & "']")
                ' Setting is missing?
                If objSettings Is Nothing AndAlso objCommon IsNot Nothing Then
                    ' Check common settings
                    objSettings = objCommon.SelectNodes("Setting[@key='" & p_strKey & "']")
                End If
            ElseIf objCommon IsNot Nothing Then
                ' Check common settings
                objSettings = objCommon.SelectNodes("Setting[@key='" & p_strKey & "']")
            Else
                Return Nothing
            End If

            ' Setting found?
            If objSettings IsNot Nothing Then
                Dim lstSettings As New List(Of String)
                For Each objSetting As XmlNode In objSettings
                    If (objSetting.NodeType <> XmlNodeType.Comment) Then
                        If objSetting.Attributes("encrypted") IsNot Nothing _
                        AndAlso objSetting.Attributes("encrypted").Value.ToLower = "true" Then
                            lstSettings.Add(Me.Decrypt(p_strKey, objSetting.InnerText))
                        Else
                            lstSettings.Add(objSetting.InnerText)
                        End If
                    End If
                Next
                Return lstSettings
            Else
                Return Nothing
            End If

        Catch ex As Exception
            Me.AddException(ex)
            Return Nothing
        End Try
    End Function

    ' This function saves the given value on the specific setting for the current customer id
    Protected Function SaveSetting(ByVal p_strKey As String, ByVal p_strValue As String) As Boolean
        Return Me.SaveSetting(Me.CustomerId, p_strKey, p_strValue)
    End Function

    ' This function saves the given value on the specific setting for the specific current customer id
    Protected Function SaveSetting(ByVal p_strCustomerId As String, ByVal p_strKey As String, ByVal p_strValue As String) As Boolean
        Try
            ' Try to load settings document
            If m_docSettings Is Nothing Then Me.LoadSettings()

            Dim objCustomer As XmlNode = m_docSettings.SelectSingleNode("Settings/CustomerSettings[@CustomerId='" & p_strCustomerId & "']")
            Dim objCommon As XmlNode = m_docSettings.SelectSingleNode("Settings/CommonSettings")
            Dim objSetting As XmlNode

            ' Check customer settings
            If objCustomer IsNot Nothing Then
                objSetting = objCustomer.SelectSingleNode("Setting[@key='" & p_strKey & "']")
                ' Setting is missing?
                If objSetting Is Nothing AndAlso objCommon IsNot Nothing Then
                    ' Check common settings
                    objSetting = objCommon.SelectSingleNode("Setting[@key='" & p_strKey & "']")
                End If
            ElseIf objCommon IsNot Nothing Then
                ' Check common settings
                objSetting = objCommon.SelectSingleNode("Setting[@key='" & p_strKey & "']")
            Else
                Return False
            End If

            ' Setting found?
            If objSetting IsNot Nothing Then
                If objSetting.Attributes("encrypt") IsNot Nothing _
                AndAlso objSetting.Attributes("encrypt").Value.ToLower = "true" Then
                    objSetting.InnerText = Me.Encrypt(p_strKey, p_strValue)

                    objSetting.Attributes.Remove(objSetting.Attributes("encrypt"))
                    objSetting.Attributes.Append(m_docSettings.CreateAttribute("encrypted"))
                    objSetting.Attributes("encrypted").Value = "true"

                ElseIf objSetting.Attributes("encrypted") IsNot Nothing _
                AndAlso objSetting.Attributes("encrypted").Value.ToLower = "true" Then
                    objSetting.InnerText = Me.Encrypt(p_strKey, p_strValue)
                Else
                    objSetting.InnerText = p_strValue
                End If
                m_docSettings.Save(m_strDocSettingsPath)
            Else
                ' Add setting
                objSetting = m_docSettings.CreateElement("Setting")
                objSetting.Attributes.Append(m_docSettings.CreateAttribute("key"))
                objSetting.Attributes("key").Value = p_strKey
                objSetting.InnerText = p_strValue

                objCustomer.AppendChild(objSetting)
                Return False
            End If

        Catch ex As Exception
            Me.AddException(ex)
            Return False
        End Try
    End Function

    ' Use this property to check if an input file is given
    Protected ReadOnly Property InputFile() As String
        Get
            Return m_strInputFile
        End Get
    End Property

    ' Use this property to return an output file
    Protected Property OutputFile() As String
        Set(ByVal value As String)
            m_strOutputFile = value
        End Set
        Get
            Return m_strOutputFile
        End Get
    End Property

    ' Use this function to add an exception to the exception list
    Protected Sub AddException(ByVal p_objException As Exception)
        m_lstExceptions.Add(p_objException.ToString())
    End Sub

    ' Use this function to add an exception to the exception list
    Protected Sub AddException(ByVal p_strMessage As String)
        m_lstExceptions.Add(p_strMessage)
    End Sub

    ' Use this function to add an warning to the warnings list
    Protected Sub AddWarning(ByVal p_strMessage As String)
        m_lstWarnings.Add(p_strMessage)
    End Sub

    ' Use this function to add information to the list
    Protected Sub AddInformation(ByVal p_strMessage As String)
        m_lstInformation.Add(p_strMessage)
    End Sub

    ' Use this function to encrypt some data
    Protected Function Encrypt(ByVal p_strKey As String, ByVal p_strValue As String) As String
        Dim objEncrypt As New Crypto()
        With objEncrypt
            .Key = p_strKey
            .Data = p_strValue
            .Encrypt()
            Return .Data()
        End With
    End Function

    ' Use this function to decrypt some data
    Protected Function Decrypt(ByVal p_strKey As String, ByVal p_strValue As String) As String
        Dim objEncrypt As New Crypto()
        With objEncrypt
            .Key = p_strKey
            .Data = p_strValue
            .Decrypt()
            Return .Data()
        End With
    End Function

    ' Use this to return the correct result level
    Protected Function CheckResultLevel() As ResultLevel
        ' Check result
        If Me.GetExceptions.Length > 0 Then
            If Me.GetWarnings.Length > 0 Then
                If Me.GetInformation.Length > 0 Then
                    Return ResultLevel.Info_AND_Warning_AND_Exception
                Else
                    Return ResultLevel.Warning_AND_Exception
                End If
            ElseIf Me.GetInformation.Length > 0 Then
                Return ResultLevel.Info_AND_Exception
            Else
                Return ResultLevel.Exception
            End If
        ElseIf Me.GetWarnings.Length > 0 Then
            If Me.GetInformation.Length > 0 Then
                Return ResultLevel.Info_AND_Warning
            Else
                Return ResultLevel.Warning
            End If
        ElseIf Me.GetInformation.Length > 0 Then
            Return ResultLevel.Information
        Else
            Return ResultLevel.Success
        End If
    End Function


    ' This function is used internally to load settings file
    Private Function LoadSettings() As Boolean
        Try
            If File.Exists(m_strDocSettingsPath) Then
                m_docSettings = New XmlDocument()
                m_docSettings.Load(m_strDocSettingsPath)

                ' Check if some setting should be encrypted
                Dim blnEncrypt As Boolean = False
                For Each objSetting As XmlNode In m_docSettings.SelectSingleNode("Settings/CommonSettings").ChildNodes
                    If (objSetting.NodeType <> XmlNodeType.Comment) Then
                        Dim strKey As String = objSetting.Attributes("key").Value
                        Dim strValue As String = objSetting.InnerText

                        If objSetting.Attributes("encrypt") IsNot Nothing _
                        AndAlso objSetting.Attributes("encrypt").Value.ToLower = "true" Then
                            objSetting.InnerText = Me.Encrypt(objSetting.Attributes("key").Value, objSetting.InnerText)
                            blnEncrypt = True

                            objSetting.Attributes.Remove(objSetting.Attributes("encrypt"))
                            objSetting.Attributes.Append(m_docSettings.CreateAttribute("encrypted"))
                            objSetting.Attributes("encrypted").Value = "true"
                        End If
                    End If
                Next

                If Me.CustomerId.Length > 0 Then
                    If m_docSettings.SelectSingleNode("Settings/CustomerSettings[@CustomerId='" & Me.CustomerId & "']") IsNot Nothing Then
                        ' Then read customer specific settings
                        For Each objSetting As XmlNode In m_docSettings.SelectSingleNode("Settings/CustomerSettings[@CustomerId='" & Me.CustomerId & "']").ChildNodes
                            If (objSetting.NodeType <> XmlNodeType.Comment) Then
                                Dim strKey As String = objSetting.Attributes("key").Value
                                Dim strValue As String = objSetting.InnerText

                                If objSetting.Attributes("encrypt") IsNot Nothing _
                                AndAlso objSetting.Attributes("encrypt").Value.ToLower = "true" Then
                                    objSetting.InnerText = Me.Encrypt(objSetting.Attributes("key").Value, objSetting.InnerText)
                                    blnEncrypt = True

                                    objSetting.Attributes.Remove(objSetting.Attributes("encrypt"))
                                    objSetting.Attributes.Append(m_docSettings.CreateAttribute("encrypted"))
                                    objSetting.Attributes("encrypted").Value = "true"
                                End If
                            End If
                        Next
                    Else
                        Me.AddException(String.Format("Customer with Id '{0}' is missing in configuration file", Me.CustomerId))
                        Return False
                    End If
                End If

                If blnEncrypt Then m_docSettings.Save(m_strDocSettingsPath)

                Return True
            Else
                Me.AddException(String.Format("Configuration file '{0}' is missing", m_strDocSettingsPath))
                Return False
            End If
        Catch ex As Exception
            Me.AddException(ex)
            Return False
        End Try
    End Function

#Region " IDisposable Support "

    ' Function that have to be overridden by child
    ' This is where release of resources should be implemented
    Public Overridable Sub Dispose() Implements System.IDisposable.Dispose
        Me.AddException(New NotImplementedException("Dispose method must be implemented by child"))
    End Sub

    ' This function is used to release internal resources
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' Free other state (managed objects).
                m_lstExceptions = Nothing
                m_lstWarnings = Nothing
                m_lstInformation = Nothing
                m_docSettings = Nothing
            End If

            ' Free your own state (unmanaged objects).

            ' Set large fields to null.
        End If

        Me.disposedValue = True
        GC.SuppressFinalize(Me)
    End Sub

#End Region

End Class
