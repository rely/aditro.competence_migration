﻿Public Module Common

    Public Enum ResultLevel As Integer
        Success = 0
        Information = 1
        Warning = 2
        Info_AND_Warning = 3
        Exception = 4
        Info_AND_Exception = 5
        Warning_AND_Exception = 6
        Info_AND_Warning_AND_Exception = 7
    End Enum

End Module
