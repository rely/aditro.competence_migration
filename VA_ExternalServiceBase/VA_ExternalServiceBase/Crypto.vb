﻿Imports System.Text
Imports System.Security.Cryptography

Friend Enum HashLength As Byte
    Bits32 = 32
    Bits128 = 128
    Bits32Legacy = 33
End Enum

Friend Class Crypto

#Region " Protected member variables "

    Protected c_strKey As String
    Protected c_strData As String
    Protected c_bytData As Byte()
    Protected c_blnDataAsString As Boolean = True

#End Region

#Region " Protected member functions "

    '<method_description name="Compute32BitHash">
    '   <description>
    '       Computes a 32 bit hash value returned as a string. This hash value can be used as a 
    '       one way encryption. It's compatible with the pre Lynx version of Persona Lön.
    '   </description>
    '   <declaration>Protected Function</declaration>
    '   <parameters>
    '       <parameter>
    '           <name>p_strData</name>
    '           <datatype>String</datatype>
    '           <declaration>ByRef</declaration>
    '           <direction>In</direction>
    '           <default_value></default_value>
    '           <description>
    '               The data for which a hash value will be computed.
    '           </description>
    '       </parameter>
    '   </parameters>
    '   <datatype>String</datatype>
    '   <created>2001-11-29</created>
    '   <author>John Bergdahl</author>
    '   <dependencies></dependencies>
    '   <exceptions></exceptions>
    '   <revisions></revisions>
    '</method_description>
    Protected Function Compute32BitHash(ByRef p_strData As String) As String
        Dim intLoop As Integer
        Dim dblCodeKey As Double
        Dim sngWeightArr(10 - 1) As Single

        ' Initialize variables
        intLoop = 0
        dblCodeKey = 0

        ' "Random numbers" from Centura application.
        ' The randomizer seed was hard coded so we can safely use these hard coded values 
        ' to provide compatibility with previous versions.
        sngWeightArr(0) = 8813
        sngWeightArr(1) = 12668
        sngWeightArr(2) = 21566
        sngWeightArr(3) = 9027
        sngWeightArr(4) = 15114
        sngWeightArr(5) = 17675.5
        sngWeightArr(6) = 13181.5
        sngWeightArr(7) = 8540
        sngWeightArr(8) = 12381
        sngWeightArr(9) = 19675

        ' Iterate through each character in the string
        For intLoop = 0 To p_strData.Length - 1
            ' Generate a value for the current character and add it to the key
            dblCodeKey = CType(dblCodeKey + Asc(p_strData.Chars(intLoop)) * sngWeightArr(intLoop Mod 10), Double)
            '            Debug.WriteLine(nCodeKey.ToString())
        Next

        Return Trim(CStr(Math.Round(dblCodeKey + 0.01, 0)))
    End Function

    '<method_description name="Compute32BitLegacyHash">
    '   <description>
    '       This is compatible with ESS versions earlier than 2.2.
    '   </description>
    '   <declaration>Protected Function</declaration>
    '   <parameters>
    '       <parameter>
    '           <name>p_strData</name>
    '           <datatype>String</datatype>
    '           <declaration>ByRef</declaration>
    '           <direction>In</direction>
    '           <default_value></default_value>
    '           <description>
    '               The data for which a hash value will be computed.
    '           </description>
    '       </parameter>
    '   </parameters>
    '   <datatype>String</datatype>
    '   <created>2008-03-05</created>
    '   <author>John Bergdahl</author>
    '   <dependencies></dependencies>
    '   <exceptions></exceptions>
    '   <revisions></revisions>
    '</method_description>
    Protected Function Compute32BitLegacyHash(ByRef p_strData As String) As String
        Dim intLoop As Integer
        Dim nCodeKey As Integer
        Dim sngWeightArr(10 - 1) As Single

        ' Initialize variables
        intLoop = 0
        nCodeKey = 0

        ' "Random numbers" from Centura application.
        ' The randomizer seed was hard coded so we can safely use these hard coded values 
        ' to provide compatibility with previous versions.
        sngWeightArr(0) = 8813
        sngWeightArr(1) = 12668
        sngWeightArr(2) = 21566
        sngWeightArr(3) = 9027
        sngWeightArr(4) = 15114
        sngWeightArr(5) = 17675.5
        sngWeightArr(6) = 13181.5
        sngWeightArr(7) = 8540
        sngWeightArr(8) = 12381
        sngWeightArr(9) = 19675

        ' Iterate through each character in the string
        For intLoop = 0 To p_strData.Length - 1
            ' Generate a value for the current character and add it to the key
            nCodeKey = CType(nCodeKey + Asc(p_strData.Chars(intLoop)) * sngWeightArr(intLoop Mod 10), Integer)
        Next

        Return nCodeKey.ToString()
    End Function


    '<method_description name="Compute128BitHash">
    '   <description>
    '       Computes a 128 bit hash value for the given string. It can be used as a one way encryption or
    '       a "finger print" of the given data. Higher security than the 32 bit version, but not compatible
    '       with the old passwords of Persona Lön.
    '   </description>
    '   <declaration></declaration>
    '   <parameters>
    '       <parameter>
    '           <name>p_strData</name>
    '           <datatype>String</datatype>
    '           <declaration>ByRef</declaration>
    '           <direction>In</direction>
    '           <default_value></default_value>
    '           <description>
    '               The data for which a hash value will be computed.
    '           </description>
    '       </parameter>
    '   </parameters>
    '   <datatype>Byte[]</datatype>
    '   <created>2001-11-29</created>
    '   <author>John Bergdahl</author>
    '   <dependencies></dependencies>
    '   <exceptions></exceptions>
    '   <revisions></revisions>
    '</method_description>
    Protected Function Compute128BitHash(ByRef p_strData As String) As Byte()
        Dim bytSource() As Byte
        Dim bytHash() As Byte

        ' Convert the string to an array of bytes, and calculate it's hash value
        bytSource = Encoding.UTF8.GetBytes(p_strData)
        bytHash = New MD5CryptoServiceProvider().ComputeHash(bytSource)

        Return bytHash
    End Function

#End Region

    '<method_description name="Key">
    '   <description>
    '       Sets and gets the secret key used for decryption and encryption. To successfully decrypt data, the
    '       same key that was used for the encryption must be used for the decryption.
    '   </description>
    '   <declaration>Friend Property</declaration>
    '   <parameters></parameters>
    '   <datatype>String</datatype>
    '   <created>2001-11-29</created>
    '   <author>John Bergdahl</author>
    '   <dependencies></dependencies>
    '   <exceptions></exceptions>
    '   <revisions></revisions>
    '</method_description>
    Friend Property Key() As String
        Get
            Return c_strKey
        End Get
        Set(ByVal Value As String)
            c_strKey = Value
        End Set
    End Property

    '<method_description name="Data">
    '   <description>
    '       Sets and gets the data to operate the cryptographic functions on.
    '   </description>
    '   <declaration>Friend Property</declaration>
    '   <parameters></parameters>
    '   <datatype>String</datatype>
    '   <created>2001-11-29</created>
    '   <author>John Bergdahl</author>
    '   <dependencies></dependencies>
    '   <exceptions></exceptions>
    '   <revisions></revisions>
    '</method_description>
    Friend Property Data() As String
        Get
            Return c_strData
        End Get
        Set(ByVal Value As String)
            c_strData = Value
        End Set
    End Property

    '<method_description name="DataAsByte">
    '   <description>
    '       Sets and gets the data to operate the cryptographic functions on.
    '   </description>
    '   <declaration>Friend Property</declaration>
    '   <parameters></parameters>
    '   <datatype>Byte()</datatype>
    '   <created>2003-06-05</created>
    '   <author>Lars Jynnesjö</author>
    '   <dependencies></dependencies>
    '   <exceptions></exceptions>
    '   <revisions></revisions>
    '</method_description>
    Friend Property DataAsByte() As Byte()
        Get
            Return c_bytData
        End Get
        Set(ByVal Value As Byte())
            c_blnDataAsString = False
            c_bytData = Value
        End Set
    End Property

    '<method_description name="GetHashValue">
    '   <description>
    '       Computes a hash key for the Data member, and returns it as a string. This can be used as a one way
    '       encryption. There is no way to calculate the source data that was used for a given hash value.
    '   </description>
    '   <declaration>Friend Function</declaration>
    '   <parameters>
    '       <parameter>
    '           <name>objLength</name>
    '           <datatype>HashLength</datatype>
    '           <declaration>ByVal</declaration>
    '           <direction>In</direction>
    '           <default_value></default_value>
    '           <description>
    '               Sets the length of the returned hash value. Use 32 bits for compatibility with 
    '               the pre Lynx Persona Lön login and password functions. Use 128 for higher security.
    '               The hash value is converted to a string either from a System.Int variable or 
    '               as a Base64 encoded Byte array. It can therefore safely be stored a normal text.
    '           </description>
    '       </parameter>
    '   </parameters>
    '   <datatype>String</datatype>
    '   <created>2001-11-29</created>
    '   <author>John Bergdahl</author>
    '   <dependencies></dependencies>
    '   <exceptions>
    '       <exception>
    '           <name>PersonaException</name>
    '           <type>tpInternalAppException</type>
    '           <description>Throws an exception if the hash length is invalid</description>
    '       </exception>
    '   </exceptions>
    '   <revisions></revisions>
    '</method_description>
    Friend Function GetHashValue(ByVal objLength As HashLength) As String
        Dim strRet As String

        ' Choose hash computation depending on objLength
        Select Case objLength
            Case HashLength.Bits32
                strRet = Compute32BitHash(Data)
            Case HashLength.Bits32Legacy
                strRet = Compute32BitLegacyHash(Data)
            Case HashLength.Bits128
                strRet = Convert.ToBase64String(Compute128BitHash(Data))
            Case Else
                Throw New Exception("Invalid hash length")
        End Select

        Return strRet
    End Function

    '<method_description name="Encrypt">
    '   <description>
    '       Decrypt the Data member using a strong DES algorithm. The encryption uses the Key member as a
    '       secret password. This same Key must be used again when decrypting the Data. The encrypted value
    '       is Base64 encoded, and can safely be stored as normal text.
    '   </description>
    '   <declaration>Friend Sub</declaration>
    '   <parameters></parameters>
    '   <datatype></datatype>
    '   <created>2001-11-29</created>
    '   <author>John Bergdahl</author>
    '   <dependencies>
    '       <dependency>System.Security.Cryptography.DESCryptoServiceProvider</dependency>
    '   </dependencies>
    '   <exceptions>
    '       <exception>
    '           <name>PersonaException</name>
    '           <type>tpInternalAppException</type>
    '           <description>Throws an exception if the secret key isn't set</description>
    '       </exception>
    '   </exceptions>
    '   <revisions></revisions>
    '</method_description>
    Friend Sub Encrypt()
        Dim DES As New DESCryptoServiceProvider()
        Dim bytKey(7) As Byte
        Dim bytTarget() As Byte

        ' Make sure there is a key value set
        If Me.Key = "" Then
            Throw New Exception("The secret key must be set before using cryptographic functions.")
        End If

        ' Set 64 bit secret key for DES algorithm
        ' Use the first 8 bytes of the Key's hash value
        System.Array.Copy(Compute128BitHash(Me.Key), bytKey, 8)
        DES.Key = bytKey
        DES.Mode = CipherMode.ECB

        ' Set initialization vector
        DES.IV = bytKey

        ' Perform encryption
        Dim objDESEncrypt As ICryptoTransform = DES.CreateEncryptor()
        If c_blnDataAsString Then
            Dim bytSource() As Byte = Encoding.UTF8.GetBytes(Me.Data)
            bytTarget = objDESEncrypt.TransformFinalBlock(bytSource, 0, bytSource.Length)

            ' Convert the encrypted bytes to a Base64 string and set the Data member
            Me.Data = Convert.ToBase64String(bytTarget)
        Else
            bytTarget = objDESEncrypt.TransformFinalBlock(c_bytData, 0, c_bytData.Length)
            Me.DataAsByte = bytTarget
        End If
    End Sub

    '<method_description name="Decrypt">
    '   <description>
    '       Decrypts the string in the Data member. The Key member is used as decryption password.
    '   </description>
    '   <declaration>Friend Sub</declaration>
    '   <parameters></parameters>
    '   <datatype></datatype>
    '   <created>2001-11-29</created>
    '   <author>John Bergdahl</author>
    '   <dependencies>
    '       <dependency>System.Security.Cryptography.DESCryptoServiceProvider</dependency>
    '   </dependencies>
    '   <exceptions>
    '       <exception>
    '           <name>PersonaException</name>
    '           <type>tpInternalAppException</type>
    '           <description>Throws an exception if the secret key isn't set</description>
    '       </exception>
    '   </exceptions>
    '   <revisions></revisions>
    '</method_description>
    Friend Sub Decrypt()
        Dim DES As New DESCryptoServiceProvider()
        Dim bytKey(7) As Byte
        Dim bytTarget() As Byte

        ' Make sure there is a key value set
        If Me.Key = "" Then
            Throw New Exception("The secret key must be set before using cryptographic functions.")
        End If

        ' Set 64 bit secret key for DES algorithm
        ' Use the first 8 bytes of the Key's hash value
        System.Array.Copy(Compute128BitHash(Me.Key), bytKey, 8)
        DES.Key = bytKey
        DES.Mode = CipherMode.ECB

        ' Set initialization vector
        DES.IV = bytKey

        ' Perform decryption
        Dim objDESDecrypt As ICryptoTransform = DES.CreateDecryptor()
        If c_blnDataAsString Then
            Dim bytSource() As Byte = Convert.FromBase64String(Me.Data)
            bytSource = Convert.FromBase64String(Me.Data)
            bytTarget = objDESDecrypt.TransformFinalBlock(bytSource, 0, bytSource.Length)
            ' Set the Data member to the decrypted string
            Me.Data = Encoding.UTF8.GetString(bytTarget)
        Else
            bytTarget = objDESDecrypt.TransformFinalBlock(c_bytData, 0, c_bytData.Length)
            Me.DataAsByte = bytTarget
        End If
    End Sub

End Class
