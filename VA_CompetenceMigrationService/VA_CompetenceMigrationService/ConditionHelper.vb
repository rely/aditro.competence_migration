﻿'Imports Persona.InfoProvider
Imports System.Text
Imports System.Xml
'Imports Persona.Common.HR
Imports VA_ExternalServiceBase

Public Class ConditionHelper

    Private m_objSQLHelper As SQLHelper
    Private c_dsResult As DataSet
    Private c_strCompany As String
    Private c_strEmpNo As String

#Region " Public methods - Single instance "

    Public Sub New(ByRef p_objSQLHelper As SQLHelper)
        m_objSQLHelper = p_objSQLHelper
    End Sub

    Public Function GetSubscriberConditionById(ByRef p_strConditionId As String) As String

        ' Fetch the condition from the condition table
        c_dsResult = m_objSQLHelper.ExecuteSelect( _
            String.Format("SELECT RADNR, VANSTER, FALT, OPER, VARDE, HOGER1, OCH_ELLER, HOGER2, FALT_TYP FROM P_PREN_VILLKOR_RAD WHERE VILLKOR_ID = '{0}' ORDER BY RADNR", p_strConditionId))

        ' Check result
        If c_dsResult.Tables(0).Rows.Count > 0 Then
            Return TranslateConditionToSQL()
        Else
            Return String.Empty
        End If
    End Function


#End Region

#Region " Private methods - General "

    Private Function TranslateConditionToSQL() As String

        Dim strCondition As New StringBuilder
        Dim drRow As DataRow
        Dim blnUsualCondition As Boolean
        Dim intCountLeftParanthesis As Integer
        Dim intCountRightParanthesis As Integer
        Dim intIndex As Integer
        Dim intCountDifferentTypes As Integer
        Dim intCountRows As Integer
        Dim strField As String
        Dim strFieldCGID As String = ""
        Dim strFieldType As String
        Dim strOper As String
        Dim strValue As String = ""
        Dim strColumnName As String
        Dim strAndOr As String
        Dim strAndOrPreviousRow As String = ""
        Dim strTypeofConditionRows() As String = Nothing      ' I = Individual, C = Competence
        Dim strOldKf As String = ""
        Dim strOldField As String = ""
        Dim strOldOper As String
        Dim strOldAndOr As String
        Dim strOldValue As String

        ' Loop thru the rows in the recordset
        strCondition.Append(" ")
        For Each drRow In c_dsResult.Tables(0).Rows

            ' check that rows are tied together with "and/or"
            intCountRows = intCountRows + 1

            ' Add a space?
            If Mid(strCondition.ToString, strCondition.Length, 1) <> " " Then strCondition.Append(" ")

            ' Set left parenthesis
            If Not IsDBNull(drRow("VANSTER")) Then strCondition.Append(CType(drRow("VANSTER"), String))

            ' Get Field (Column name)
            strField = CType(drRow("FALT"), String)

            'EV: F51
            If strField = "PU.COMPETENCEID" Then strField = "PU.UTBKOMP_ID"
            If strField = "PU.KURSID" Then strField = "PU.KURS_ID"
            If strField = "PU.BEFATTNID" Then strField = "PU.BEFATTN_ID"

            ' Get Fieldtype
            strFieldType = CType(drRow("FALT_TYP"), String)

            ' Check if Usual Condition / Conditional formula
            ' (Conditional formulas only exists for authorisation condititons)
            blnUsualCondition = True

            ' Get operator
            strOper = GetOper(drRow)

            ' Check if Usual Condition / Conditional formula
            If blnUsualCondition = True Then
                ' Get value
                strValue = GetValue(drRow)

                If strFieldCGID <> "" Then
                    ' Condition against COMPETENCE
                    strCondition.Append(" (KF_ID = " & strFieldCGID & " AND " & _
                                        strField & strOper & strValue & ")")

                    If SubSelectNeeded(strOper, strValue) Then
                        '050406 Tagit bort koll pе strTypePreviousRow <> "" AndAlso subselect direkt borde inte skapa nеgot fel?
                        'Lagt till koll pе kolumn, mеste ha subselect om det дr nyckelkolumn
                        '050408 Lagt till blnMultipleKF i if, behцvs ingen subselect om det bara finns en komp-rad? "AndAlso blnMultipleKF"
                        If Not (strFieldCGID = strOldKf AndAlso strField = strOldField AndAlso strOper <> " = " AndAlso Not (strField = "PU.KURS_ID" Or strField = "PU.BEFATTN_ID" Or strField = "PU.UTBKOMP_ID")) Then
                            strCondition.Append(" AND NOT EXISTS (SELECT PU.FORETAG FROM PERSON_UK PU" & _
                                               " WHERE PU.FORETAG = P.FORETAG AND PU.ANSTNR = P.ANSTNR " & _
                                                " AND KF_ID = " & strFieldCGID & " AND " & strField & " = " & strValue & ")")
                        End If
                    End If
                Else
                    ' Check if condition is set against:
                    ' PERSON / P_PERSON_BEGREPP OR ANSTARENDE / RA_ANSAREN_BEGREPP
                    intIndex = InStr(strField, ".")
                    If Left(strField, intIndex) = "PB." Then
                        ' Condition against P_PERSON_BEGREPP
                        ' Then the sql-clause will be more complex

                        ' Get column name
                        strColumnName = Mid(strField, intIndex + 1)

                        ' Create condition
                        If SubSelectNeeded(strOper, strValue) Then
                            strCondition.Append("((")
                        Else
                            strCondition.Append("(")
                        End If

                        Select Case strFieldType
                            Case "S", "D"
                                ' String
                                strField = "PB.VARDE"
                            Case "N"
                                strField = "PB.VARDE_NUM"
                        End Select

                        strCondition.Append(" EXISTS (SELECT PB.FORETAG FROM P_PERSON_BEGREPP PB" & _
                                            " WHERE PB.FORETAG = P.FORETAG AND PB.ANSTNR = P.ANSTNR " & _
                                            " AND PB.KOLUMN_NAMN = '" & strColumnName & "'" & _
                                            " AND " & strField & strOper & strValue & "))")

                        If SubSelectNeeded(strOper, strValue) Then
                            strCondition.Append(" OR 0 = (SELECT COUNT(*) FROM P_PERSON_BEGREPP PB")
                            strCondition.Append(" WHERE P.FORETAG = PB.FORETAG AND P.ANSTNR = PB.ANSTNR")
                            strCondition.Append(" AND PB.KOLUMN_NAMN = '" & strColumnName & "'))")
                        End If

                    ElseIf Left(strField, intIndex) = "RB." Then
                        ' Condition against RA_ANSAREN_BEGREPP
                        ' Then the sql-clause will be more complex

                        ' Get column name
                        strColumnName = Mid(strField, intIndex + 1)

                        ' Create condition
                        If SubSelectNeeded(strOper, strValue) Then
                            strCondition.Append("((")
                        Else
                            strCondition.Append("(")
                        End If

                        Select Case strFieldType
                            Case "S", "D"
                                ' String
                                strField = "RB.VARDE"
                            Case "N"
                                strField = "RB.VARDE_NUM"
                        End Select

                        strCondition.Append(" RA.ANSTARENDE IN (SELECT RB.ANSTARENDE FROM RA_ANSAREN_BEGREPP RB" & _
                                            " WHERE RB.ANSTARENDE = RA.ANSTARENDE " & _
                                            " AND RB.KOLUMN_NAMN = '" & strColumnName & "'" & _
                                            " AND " & strField & strOper & strValue & "))")

                        If SubSelectNeeded(strOper, strValue) Then
                            strCondition.Append(" OR (SELECT COUNT(*) FROM RA_ANSAREN_BEGREPP RB")
                            strCondition.Append(" WHERE RA.ANSTARENDE = RB.ANSTARENDE")
                            strCondition.Append(" AND RB.KOLUMN_NAMN = '" & strColumnName & "'")
                            strCondition.Append(") = 0)")
                        End If
                    Else
                        ' Condition against PERSON or ANSTARENDE
                        strCondition.Append(strField & strOper & strValue)
                    End If
                End If
            Else
                ' TODO
                ' Conditional formula - Call method to translate the formula
                'strCondition.Append("(")
                'strCondition.Append(GetConditionalFormula(strField, strConditionFormulaID, strOper))
                'strCondition.Append(")")
            End If

            ' Set inner right parenthesis
            If Not IsDBNull(drRow("HOGER1")) Then strCondition.Append(CType(drRow("HOGER1"), String))

            ' Sets AND/OR if the condition contains more than the current row
            strAndOr = ""
            If Not IsDBNull(drRow("OCH_ELLER")) Then
                strAndOr = CType(drRow("OCH_ELLER"), String)
                If strAndOr.ToUpper = "OCH" Then
                    strCondition.Append(" AND ")
                Else
                    strCondition.Append(" OR ")
                End If
            End If
            strAndOrPreviousRow = strAndOr

            '050405
            strOldKf = strFieldCGID
            strOldField = strField
            strOldOper = strOper
            strOldAndOr = strAndOr
            strOldValue = strValue

            ' Set outer right parenthesis
            If Not IsDBNull(drRow("HOGER2")) Then strCondition.Append(CType(drRow("HOGER2"), String))
        Next

        ' Do some final checks that the condition is correct
        If strCondition.Length > 1 Then
            ' check the number of parenthesis
            CountNoOfParenthesis(strCondition.ToString, intCountLeftParanthesis, intCountRightParanthesis)
            'If Not intCountLeftParanthesis = intCountRightParanthesis Then Throw New PersonaException(TypeOfException.tpValidationException, Translations.GetMessages("2808;3026"))

            ' check the ev. combination of individuals- and competence-rows
            If IsArray(strTypeofConditionRows) Then
                For intIndex = 0 To strTypeofConditionRows.GetUpperBound(0)
                    If intIndex > 0 Then
                        ' add to counter if the types are different on current row and previous row
                        If strTypeofConditionRows(intIndex) <> strTypeofConditionRows(intIndex - 1) Then
                            intCountDifferentTypes = intCountDifferentTypes + 1
                        End If
                    End If
                Next

                ' if counter > 1, then we got a combination like this:
                ' Ind + Comp + Ind, which is not allowed! All individual-information
                ' must appear in the beginning, it will do later handling much easier.
                'If intCountDifferentTypes > 1 Then Throw New PersonaException(TypeOfException.tpValidationException, Translations.GetMessages("2808;3027"))
            End If
        End If

        If strCondition.Length > 1 Then
            ' Set an outer parenthesis around the complete condition
            strCondition.Insert(0, "(")
            strCondition.Append(")")
        End If

        Return strCondition.ToString

    End Function


    Private Function GetOper(ByRef p_drRow As DataRow) As String

        Select Case CType(p_drRow("OPER"), String).ToUpper
            Case "=TOM"
                Return " IS "
            Case "INTE=TOM"
                Return " IS "
            Case "MELLAN"
                Return " BETWEEN "
            Case "BÖRJAR PÅ"
                Return " LIKE "
            Case "SOM"
                Return " LIKE "
            Case "INTE SOM"
                Return " NOT LIKE "
            Case "INTE ="
                Return " <> "
            Case "INTE >"
                Return " <= "
            Case "INTE <"
                Return " >= "
            Case Else
                Return " " & CType(p_drRow("OPER"), String) & " "
        End Select
    End Function

    Private Function GetValue(ByRef p_drRow As DataRow) As String

        Dim strOper As String
        Dim strValue As String
        Dim strField As String
        Dim strFieldType As String
        Dim strLVal As String
        Dim strRVal As String

        ' Get operator
        strOper = CType(p_drRow("OPER"), String).ToUpper
        If strOper = "=TOM" Then
            ' "=tom" is a special case (IS NULL)
            Return " NULL "
        ElseIf strOper = "INTE=TOM" Then
            ' "inte=tom" is a special case (IS NOT NULL)
            Return " NOT NULL "
        Else
            If IsDBNull(p_drRow("VARDE")) Then Return Nothing
            If IsDBNull(p_drRow("FALT")) Then Return Nothing
            If IsDBNull(p_drRow("FALT_TYP")) Then Return Nothing

            strValue = CType(p_drRow("VARDE"), String)
            strField = CType(p_drRow("FALT"), String)
            strFieldType = CType(p_drRow("FALT_TYP"), String)
            If strOper = "MELLAN" Then
                ' ("mellan") is a special case since it contains
                ' two values to compare with (BETWEEN)
                strLVal = Left(strValue, InStr(strValue, " och ") - 1)
                strRVal = Mid(strValue, InStr(strValue, " och ") + 5)

                AdjustValue(strField, strFieldType, strLVal)
                AdjustValue(strField, strFieldType, strRVal)
                Return strLVal & " AND " & strRVal
            ElseIf strOper = "SOM" Or strOper = "INTE SOM" Or strOper = "BÖRJAR PÅ" Then
                ' "som", "inte som" and "bцrjar pе" is special cases
                ' (* in the value field should be changed to %)
                If strFieldType = "S" Then strValue = Replace(strValue, "*", "%")
                AdjustValue(strField, strFieldType, strValue)
                Return strValue
            Else
                ' No special case
                AdjustValue(strField, strFieldType, strValue)
                Return strValue
            End If
        End If
    End Function

    Private Function SubSelectNeeded( _
                        ByRef p_strOper As String, _
                        ByRef p_strValue As String) As Boolean

        Select Case p_strOper
            Case " = ", " LIKE ", " > ", " >= ", " BETWEEN "
                Return False
            Case " <> ", " NOT LIKE ", " < ", " <= "
                Return True
            Case " IS "
                If p_strValue = " NULL " Then
                    Return True
                Else
                    Return False
                End If
            Case Else
                Return False
        End Select
    End Function

    'Private Function GetConditionalFormula( _
    '                    ByRef p_strField As String, _
    '                    ByRef p_strConditionalID As String, _
    '                    ByRef p_strOper As String) As String

    '    Dim dsConcepts As DataSet
    '    Dim strCompany As String
    '    Dim strEmpNo As String

    '    ' Fetch User-identity (Company and EmpNo)
    '    'If c_strCompany = String.Empty And c_strEmpNo = String.Empty Then
    '    '    objUser.GetIndividualID(strCompany, strEmpNo)
    '    'Else
    '    strCompany = c_strCompany
    '    strEmpNo = c_strEmpNo
    '    'End If

    '    ' Create object to receive data from database
    '    dsConcepts = m_objSQLHelper.ExecuteSelect( _
    '        String.Format("SELECT LOPNR, P_KOLUMN_NAMN FROM BE_VLKVARDE_BEGR WHERE VLKVARDE_ID = '{0}' AND OBJEKT_ID = '{1}' ORDER BY LOPNR", p_strConditionalID, c_udtObject))

    '    ' Weґve got something in return - Letґs translate it
    '    Return BuildConditionalFormula(strCompany, strEmpNo, p_strField, p_strOper, dsConcepts)

    'End Function

    'Private Function BuildConditionalFormula( _
    '                    ByVal p_strCompany As String, _
    '                    ByVal p_strEmpNo As String, _
    '                    ByVal p_strField As String, _
    '                    ByVal p_strOper As String, _
    '                    ByVal p_dsConcepts As DataSet) As String

    '    Dim drRow As DataRow
    '    Dim colConcepts As New Collection
    '    Dim objConceptValue As ConceptValueHolder
    '    Dim intIndex As Integer
    '    Dim strCondition As New StringBuilder

    '    Dim strConcept As String
    '    Dim strTable As String
    '    Dim strDatatype As String
    '    Dim strField As String
    '    Dim strValue As String
    '    Dim blnIsNull As Boolean

    '    ' Move all the concepts to a two-dimensional property bag
    '    ' (This bag should finally contain the Concept, the Value and the Datatype)
    '    'ReDim strConceptsArr(4, p_dsConcepts.Tables(0).Rows.Count - 1)

    '    ' First move the concepts
    '    intIndex = 0
    '    For Each drRow In p_dsConcepts.Tables(0).Rows
    '        If Not IsDBNull(drRow("P_KOLUMN_NAMN")) Then
    '            objConceptValue = New ConceptValueHolder
    '            objConceptValue.Concept = CType(drRow("P_KOLUMN_NAMN"), String)
    '            colConcepts.Add(objConceptValue, CType(drRow("P_KOLUMN_NAMN"), String))
    '        End If
    '    Next

    '    ' Fetch the conditional values for the identity and the datatypes
    '    objFetch = New ConditionsFetch
    '    objFetch.AddConditionalConceptValues(p_strCompany, p_strEmpNo, colConcepts)
    '    objFetch = Nothing

    '    For Each objConceptValue In colConcepts
    '        With objConceptValue
    '            If strCondition.Length > 0 Then strCondition.Append(" OR ")

    '            ' drop prefix on field, init table-prefix
    '            If InStr(1, p_strField, "P.", vbTextCompare) > 0 Then
    '                p_strField = Replace(p_strField, "P.", "")
    '                strTable = "P"
    '            ElseIf InStr(1, p_strField, "PB.", vbTextCompare) > 0 Then
    '                p_strField = Replace(p_strField, "PB.", "")
    '                strTable = "PB"
    '            ElseIf InStr(1, p_strField, "RA.", vbTextCompare) > 0 Then
    '                p_strField = Replace(p_strField, "RA.", "")
    '                strTable = "RA"
    '            ElseIf InStr(1, p_strField, "RB.", vbTextCompare) > 0 Then
    '                p_strField = Replace(p_strField, "RB.", "")
    '                strTable = "RB"
    '            End If

    '            strConcept = p_strField
    '            strValue = .Value
    '            strDatatype = .Datatype
    '            If strValue.Length = 0 Then
    '                blnIsNull = True
    '            Else
    '                blnIsNull = False
    '            End If

    '            ' Adjust string values with '', Set dates in valid format etc.
    '            AdjustValue(strConcept, strDatatype, strValue)
    '            If strTable = "P" _
    '            OrElse strTable = "RA" Then
    '                ' The table PERSON
    '                ' - The condition consists of only one row
    '                If blnIsNull Then
    '                    ' When value is null/missing, create condition which wonґt be fulfilled
    '                    'strCondition.Append("0 = 1")
    '                    'TFS1955195
    '                    'strCondition.Append(strTable & "." & strConcept & " IS NOT NULL")
    '                    strCondition.Append(strTable & "." & strConcept & " IS NOT NULL AND " & strTable & "." & strConcept & " IS NULL")
    '                Else
    '                    strCondition.Append(strTable & "." & strConcept & p_strOper & strValue)
    '                End If
    '            Else
    '                ' The table P_PERSON_BEGREPP
    '                ' - The condition is more complex
    '                'ID 7169 
    '                If strTable = "PB" Then
    '                    strCondition.Append(" EXISTS (SELECT PB.FORETAG FROM P_PERSON_BEGREPP PB" & _
    '                                            " WHERE PB.FORETAG = P.FORETAG AND PB.ANSTNR = P.ANSTNR " & _
    '                                            " AND " & strTable & ".KOLUMN_NAMN = " & SurroundString(strConcept, "'"))
    '                ElseIf strTable = "RB" Then
    '                    strCondition.Append(" EXISTS (SELECT RB.ANSTARENDE FROM RA_ANSAREN_BEGREPP RB" & _
    '                                            " WHERE RB.ANSTARENDE = RA.ANSTARENDE" & _
    '                                            " AND " & strTable & ".KOLUMN_NAMN = " & SurroundString(strConcept, "'"))
    '                End If

    '                strCondition.Append(" AND ")
    '                If blnIsNull = True Then
    '                    ' When value is null/missing, create condition which wonґt be fulfilled
    '                    'ID 7169
    '                    'strCondition.Append("0 = 1)")
    '                    'TFS1955195
    '                    Select Case strDatatype
    '                        Case "A", "D"
    '                            ' String
    '                            strField = strTable & ".VARDE"
    '                        Case "N"
    '                            strField = strTable & ".VARDE_NUM"
    '                    End Select
    '                    'strCondition.Append(strField & " IS NOT NULL" & ")")
    '                    strCondition.Append(strField & " IS NOT NULL" & " AND " & strField & " IS NULL" & ")")
    '                Else
    '                    Select Case strDatatype
    '                        Case "A", "D"
    '                            ' String
    '                            strField = strTable & ".VARDE"
    '                        Case "N"
    '                            strField = strTable & ".VARDE_NUM"
    '                    End Select
    '                    'ID 7169
    '                    strCondition.Append(strField & p_strOper & strValue & ")")
    '                End If
    '            End If
    '        End With
    '    Next

    '    Return strCondition.ToString

    'End Function

    Private Sub AdjustValue( _
                    ByRef p_strField As String, _
                    ByRef p_strFieldType As String, _
                    ByRef p_strValue As String)

        Dim intPosition As Integer

        Select Case p_strFieldType
            Case "A", "S"
                ' String
                p_strValue = "'" & p_strValue & "'"
            Case "D"
                ' Date - these values are handled differently depending on
                ' which table they will be compared against
                intPosition = InStr(p_strField, ".")
                If Left(p_strField, intPosition) = "P." _
                Or Left(p_strField, intPosition) = "PU." Then
                    ' If the field contains a constant, i.e. 'CurrentDate', convert to proper value
                    If p_strValue = "CurrentDate" Then
                        p_strValue = DateTime.Now.ToString("yyyyMMdd")
                    End If

                    ' If date is for sql-where-clause...
                    Dim value As DateTime = DateTime.Parse(Left(p_strValue, 4) & "-" & Mid(p_strValue, 5, 2) & "-" & Mid(p_strValue, 7, 2))
                    p_strValue = "'" & value.ToString("yyyy-MM-dd") & "'"
                Else
                    ' Create a quoted string
                    p_strValue = "'" & p_strValue & "'"
                End If
            Case Else
                ' Number - replace ev. decimal-comma to decimal-point
                p_strValue = Replace(p_strValue, ",", ".")
        End Select
    End Sub

    Private Sub CountNoOfParenthesis( _
                    ByRef p_strStringWithParenthesis As String, _
                    ByRef p_intLeftParenthesis As Integer, _
                    ByRef p_intRightParenthesis As Integer)

        p_intLeftParenthesis = p_strStringWithParenthesis.Split(CType("(", Char)).GetUpperBound(0) + 1
        p_intRightParenthesis = p_strStringWithParenthesis.Split(CType(")", Char)).GetUpperBound(0) + 1
    End Sub

#End Region

End Class
