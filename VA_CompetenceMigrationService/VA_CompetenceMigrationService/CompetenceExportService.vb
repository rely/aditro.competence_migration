﻿Imports System.Runtime.InteropServices
Imports System.IO
Imports System.Text
Imports System.Xml
Imports System.Data.OleDb
Imports VA_ExternalServiceBase

<GuidAttribute("02B33ED4-60CB-4E01-B575-1F16C6744E35"), ClassInterface(ClassInterfaceType.AutoDual)>
Public Class CompetenceExportService : Inherits ExternalServicesBase

    ' Used when writing text files

    Public Overrides Function ExecuteService() As ResultLevel
        Try
            Dim intResult As Integer

            Dim strFilePath As String = MyBase.InputFile
            If Not File.Exists(strFilePath) Then
                MyBase.AddException(String.Format("File '{0}' is missing", strFilePath))
                'ElseIf strConnection.Trim.Length = 0 Then
                '    MyBase.AddException("Connection string is missing")
            Else
                ' Start processing the file
                Me.WriteAllDataXmlFile(strFilePath)
            End If

            ' Return result
            Return MyBase.CheckResultLevel()

        Catch ex As Exception
            MyBase.AddException(ex)
        End Try

    End Function

#Region " Writing Xml files "

    Protected Function WriteAllDataXmlFile(ByRef p_strFilePath As String) As Integer
        Dim objStream As New StreamReader(p_strFilePath, System.Text.Encoding.Default)
        Dim strRecord As String = ""
        Dim xmlWriter As XmlTextWriter
        Dim intCount As Integer

        Try
            ' Create Xml document
            xmlWriter = Me.WriteXmlHeader()
            ' Read all the records
            strRecord = objStream.ReadLine()
            While strRecord IsNot Nothing
                ' Encapsulate record
                Dim objCourseRecord As New CompetenceRecord(strRecord)

                ' Write concepts
                Me.WriteXmlConcepts(xmlWriter, objCourseRecord)

                intCount += 1

                xmlWriter.WriteEndElement()
                xmlWriter.Flush()
                strRecord = objStream.ReadLine()
            End While
            ' Close Xml document
            Me.WriteXmlFooter(xmlWriter)
            Return intCount
        Catch ex As Exception
            MyBase.AddException(ex)
            'MyBase.AddWarning(String.Format("Fel vid post: {0} {1}", strCourseId, strSocSecNo))
        End Try

    End Function

    Protected Function WriteXmlHeader() As XmlTextWriter
        Dim strXmlEncoding As String = MyBase.GetSetting("XmlFileEncoding")
        Dim strOutputfile As String = MyBase.GetSetting("OutputFile")

        Dim xmlWriter As New XmlTextWriter(strOutputfile, Encoding.GetEncoding(strXmlEncoding))

        xmlWriter.WriteStartDocument()
        xmlWriter.Formatting = Formatting.Indented
        xmlWriter.Indentation = 2

        xmlWriter.WriteStartElement("ArrayOfCompetencePersonData")
        xmlWriter.WriteAttributeString("version", "1.1")
        xmlWriter.WriteAttributeString("name", "Aditro Competence")
        xmlWriter.WriteAttributeString("created", Format(Today, "yyyy-MM-dd") & "T" & Format(TimeOfDay, "HH:mm:ss"))
        xmlWriter.WriteAttributeString("xmlns", "http://rm.tietoenator.com/public/xml/schemas/2004/Personec")

        Return xmlWriter
    End Function

    Protected Sub WriteXmlConcepts(ByRef p_objXmlWriter As XmlTextWriter, ByRef p_objCourseRecord As CompetenceRecord)
        ' Concepts
        p_objXmlWriter.WriteStartElement("CompetencePersonData")
        p_objXmlWriter.WriteElementString("CompanyNo", p_objCourseRecord.Foretag)
        p_objXmlWriter.WriteElementString("PersonCompanyEmploymentNo", p_objCourseRecord.Anstnr)
        p_objXmlWriter.WriteElementString("CompetenceLibraryId", p_objCourseRecord.KF_Id)
        p_objXmlWriter.WriteElementString("CompetenceId", p_objCourseRecord.Utbkomp_Id)
        p_objXmlWriter.WriteElementString("Level", p_objCourseRecord.Kolumn2_Id)
        p_objXmlWriter.WriteStartElement("TargetLevel")
        p_objXmlWriter.WriteAttributeString("xsi", "nil", System.Xml.Schema.XmlSchema.InstanceNamespace, "true")
        p_objXmlWriter.WriteEndElement()

        p_objXmlWriter.WriteStartElement("FieldValues")
        p_objXmlWriter.WriteStartElement("FieldValue")
        p_objXmlWriter.WriteElementString("FieldName", "Competence.Competence_Date1")
        p_objXmlWriter.WriteElementString("Value", p_objCourseRecord.Datum_1)
        p_objXmlWriter.WriteEndElement()
        p_objXmlWriter.WriteEndElement()

        p_objXmlWriter.WriteStartElement("FieldValues")
        p_objXmlWriter.WriteStartElement("FieldValue")
        p_objXmlWriter.WriteElementString("FieldName", "Competence.Competence_Date2")
        p_objXmlWriter.WriteElementString("Value", p_objCourseRecord.Datum_2)
        p_objXmlWriter.WriteEndElement()
        p_objXmlWriter.WriteEndElement()

    End Sub

    Protected Sub WriteXmlFooter(ByRef p_objXmlWriter As XmlTextWriter)
        p_objXmlWriter.WriteEndDocument()
        p_objXmlWriter.Close()
    End Sub

#End Region

#Region " Value formatting "

    ' Adds leading zeroes to CompanyId and Employment number
    Protected Function GetFormattedID(ByVal p_strValue As String, ByVal p_intMinLength As Integer) As String
        If IsNumeric(p_strValue) _
        AndAlso p_intMinLength > 0 _
        AndAlso p_strValue.Length < p_intMinLength Then
            Return CInt(p_strValue).ToString().PadLeft(p_intMinLength, "0")
        Else
            Return p_strValue
        End If
    End Function


#End Region

#Region " IDisposable Support "

    ' To detect redundant calls
    Private disposedValue As Boolean = False

    ' This is where release of resources should be implemented
    Public Overrides Sub Dispose()
        ' IMPORTANT - Do not change this code!
        Me.Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    ' This function is used to release internal resources
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' Free other state (managed objects).

            End If

            ' Free your own state (unmanaged objects).
            ' Set large fields to null.
        End If

        ' IMPORTANT - Free base class resources as well
        MyBase.Dispose(disposing)

        Me.disposedValue = True
    End Sub
#End Region

End Class
