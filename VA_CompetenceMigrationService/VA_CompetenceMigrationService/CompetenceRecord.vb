﻿Public Class CompetenceRecord

    Public ReadOnly Foretag As String
    Public ReadOnly Anstnr As String
    Public ReadOnly KF_Id As String
    Public ReadOnly Rad As String
    Public ReadOnly Utbkomp_Id As String
    Public ReadOnly Kolumn2_Id As String
    Public ReadOnly Kolumn3_Id As String
    Public ReadOnly Status_Id As String
    Public ReadOnly Kurs_Id As String
    Public ReadOnly Kurstillfalle As String
    Public ReadOnly Befattn_Id As String
    Public ReadOnly Antal As String
    Public ReadOnly Datum_1 As String
    Public ReadOnly Datum_2 As String
    Public ReadOnly Belopp_1 As String
    Public ReadOnly Belopp_2 As String
    Public ReadOnly Fritext As String
    Public ReadOnly Andr_Datum As String
    Public ReadOnly Ander_Sign As String
    Public ReadOnly Ant_Tim As String
    Public ReadOnly Tot_Tid As String
    Public ReadOnly Medelresultat As String
    Public ReadOnly Version As String
    Public ReadOnly Personkurs_Id As String
    Public ReadOnly Ant_Forsok As String

    Public Sub New(ByRef p_strRecord As String)

        Dim arrRecordDetails() As String = p_strRecord.Split(";")

        Me.Foretag = arrRecordDetails(0)
        Me.Anstnr = arrRecordDetails(1)
        Me.KF_Id = arrRecordDetails(2)
        Me.Rad = arrRecordDetails(3)
        Me.Utbkomp_Id = arrRecordDetails(4)
        Me.Kolumn2_Id = arrRecordDetails(5)
        Me.Kolumn3_Id = arrRecordDetails(6)
        Me.Status_Id = arrRecordDetails(7)
        Me.Kurs_Id = arrRecordDetails(8)
        Me.Kurstillfalle = arrRecordDetails(9)
        Me.Befattn_Id = arrRecordDetails(10)
        Me.Antal = arrRecordDetails(11)
        Me.Datum_1 = arrRecordDetails(12)
        Me.Datum_2 = arrRecordDetails(13)
        Me.Belopp_1 = arrRecordDetails(14)
        Me.Belopp_2 = arrRecordDetails(15)
        Me.Fritext = arrRecordDetails(16)
        Me.Andr_Datum = arrRecordDetails(17)
        Me.Ander_Sign = arrRecordDetails(18)
        Me.Ant_Tim = arrRecordDetails(19)
        Me.Tot_Tid = arrRecordDetails(2)
        Me.Medelresultat = arrRecordDetails(21)
        Me.Version = arrRecordDetails(22)
        Me.Personkurs_Id = arrRecordDetails(23)
        Me.Ant_Forsok = arrRecordDetails(24)
    End Sub

End Class
