﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class VA_ServiceTestForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblAssembly = New System.Windows.Forms.Label
        Me.txtAssembly = New System.Windows.Forms.TextBox
        Me.btnBrowse = New System.Windows.Forms.Button
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.btnExecute = New System.Windows.Forms.Button
        Me.txtResult = New System.Windows.Forms.RichTextBox
        Me.lblResult = New System.Windows.Forms.Label
        Me.btnBrowse2 = New System.Windows.Forms.Button
        Me.txtFilename = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtCustomerId = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.btnDisplayOutputFile = New System.Windows.Forms.Button
        Me.btnDisplayInputFile = New System.Windows.Forms.Button
        Me.btnExportResult = New System.Windows.Forms.Button
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        Me.SuspendLayout()
        '
        'lblAssembly
        '
        Me.lblAssembly.AutoSize = True
        Me.lblAssembly.Location = New System.Drawing.Point(13, 13)
        Me.lblAssembly.Name = "lblAssembly"
        Me.lblAssembly.Size = New System.Drawing.Size(54, 13)
        Me.lblAssembly.TabIndex = 0
        Me.lblAssembly.Text = "Assembly:"
        '
        'txtAssembly
        '
        Me.txtAssembly.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtAssembly.Location = New System.Drawing.Point(16, 30)
        Me.txtAssembly.Name = "txtAssembly"
        Me.txtAssembly.Size = New System.Drawing.Size(669, 20)
        Me.txtAssembly.TabIndex = 1
        '
        'btnBrowse
        '
        Me.btnBrowse.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBrowse.Location = New System.Drawing.Point(692, 27)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(75, 23)
        Me.btnBrowse.TabIndex = 2
        Me.btnBrowse.Text = "Browse..."
        Me.btnBrowse.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'btnExecute
        '
        Me.btnExecute.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExecute.Location = New System.Drawing.Point(691, 177)
        Me.btnExecute.Name = "btnExecute"
        Me.btnExecute.Size = New System.Drawing.Size(75, 23)
        Me.btnExecute.TabIndex = 7
        Me.btnExecute.Text = "Execute"
        Me.btnExecute.UseVisualStyleBackColor = True
        '
        'txtResult
        '
        Me.txtResult.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtResult.Location = New System.Drawing.Point(12, 179)
        Me.txtResult.Name = "txtResult"
        Me.txtResult.ReadOnly = True
        Me.txtResult.Size = New System.Drawing.Size(673, 363)
        Me.txtResult.TabIndex = 8
        Me.txtResult.Text = ""
        '
        'lblResult
        '
        Me.lblResult.AutoSize = True
        Me.lblResult.Location = New System.Drawing.Point(13, 163)
        Me.lblResult.Name = "lblResult"
        Me.lblResult.Size = New System.Drawing.Size(40, 13)
        Me.lblResult.TabIndex = 5
        Me.lblResult.Text = "Result:"
        '
        'btnBrowse2
        '
        Me.btnBrowse2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBrowse2.Location = New System.Drawing.Point(692, 109)
        Me.btnBrowse2.Name = "btnBrowse2"
        Me.btnBrowse2.Size = New System.Drawing.Size(75, 23)
        Me.btnBrowse2.TabIndex = 5
        Me.btnBrowse2.Text = "Browse..."
        Me.btnBrowse2.UseVisualStyleBackColor = True
        '
        'txtFilename
        '
        Me.txtFilename.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtFilename.Location = New System.Drawing.Point(16, 112)
        Me.txtFilename.Name = "txtFilename"
        Me.txtFilename.Size = New System.Drawing.Size(669, 20)
        Me.txtFilename.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 95)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "File name:"
        '
        'txtCustomerId
        '
        Me.txtCustomerId.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCustomerId.Location = New System.Drawing.Point(16, 71)
        Me.txtCustomerId.Name = "txtCustomerId"
        Me.txtCustomerId.Size = New System.Drawing.Size(177, 20)
        Me.txtCustomerId.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(13, 54)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(66, 13)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Customer Id:"
        '
        'btnDisplayOutputFile
        '
        Me.btnDisplayOutputFile.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDisplayOutputFile.Enabled = False
        Me.btnDisplayOutputFile.Location = New System.Drawing.Point(691, 206)
        Me.btnDisplayOutputFile.Name = "btnDisplayOutputFile"
        Me.btnDisplayOutputFile.Size = New System.Drawing.Size(75, 23)
        Me.btnDisplayOutputFile.TabIndex = 9
        Me.btnDisplayOutputFile.Text = "Display file"
        Me.btnDisplayOutputFile.UseVisualStyleBackColor = True
        '
        'btnDisplayInputFile
        '
        Me.btnDisplayInputFile.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDisplayInputFile.Enabled = False
        Me.btnDisplayInputFile.Location = New System.Drawing.Point(692, 138)
        Me.btnDisplayInputFile.Name = "btnDisplayInputFile"
        Me.btnDisplayInputFile.Size = New System.Drawing.Size(75, 23)
        Me.btnDisplayInputFile.TabIndex = 6
        Me.btnDisplayInputFile.Text = "Display file"
        Me.btnDisplayInputFile.UseVisualStyleBackColor = True
        '
        'btnExportResult
        '
        Me.btnExportResult.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExportResult.Enabled = False
        Me.btnExportResult.Location = New System.Drawing.Point(691, 519)
        Me.btnExportResult.Name = "btnExportResult"
        Me.btnExportResult.Size = New System.Drawing.Size(75, 23)
        Me.btnExportResult.TabIndex = 10
        Me.btnExportResult.Text = "Save result"
        Me.btnExportResult.UseVisualStyleBackColor = True
        '
        'VA_ServiceTestForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 564)
        Me.Controls.Add(Me.btnExportResult)
        Me.Controls.Add(Me.btnDisplayInputFile)
        Me.Controls.Add(Me.btnDisplayOutputFile)
        Me.Controls.Add(Me.txtCustomerId)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnBrowse2)
        Me.Controls.Add(Me.txtFilename)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblResult)
        Me.Controls.Add(Me.txtResult)
        Me.Controls.Add(Me.btnExecute)
        Me.Controls.Add(Me.btnBrowse)
        Me.Controls.Add(Me.txtAssembly)
        Me.Controls.Add(Me.lblAssembly)
        Me.Name = "VA_ServiceTestForm"
        Me.Text = "VA_ServiceTestForm"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblAssembly As System.Windows.Forms.Label
    Friend WithEvents txtAssembly As System.Windows.Forms.TextBox
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents btnExecute As System.Windows.Forms.Button
    Friend WithEvents txtResult As System.Windows.Forms.RichTextBox
    Friend WithEvents lblResult As System.Windows.Forms.Label
    Friend WithEvents btnBrowse2 As System.Windows.Forms.Button
    Friend WithEvents txtFilename As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtCustomerId As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnDisplayOutputFile As System.Windows.Forms.Button
    Friend WithEvents btnDisplayInputFile As System.Windows.Forms.Button
    Friend WithEvents btnExportResult As System.Windows.Forms.Button
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog

End Class
