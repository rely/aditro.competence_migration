﻿Imports System.Reflection
Imports System.Text
Imports System.IO

Public Class VA_ServiceTestForm

    Private Const strBASECLASS As String = "VA_ExternalServiceBase.ExternalServicesBase"
    Private Const strTIMESTAMP_FORMAT As String = "yyyy-MM-dd HH:mm:ss.fff"

    Private m_strOutputFile As String


    Private Sub VA_ServiceTestForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If My.Settings.AssemblyPath.Length > 0 Then
            txtAssembly.Text = My.Settings.AssemblyPath
        End If
        If My.Settings.CustomerId.Length > 0 Then
            txtCustomerId.Text = My.Settings.CustomerId
        End If
        If My.Settings.FilePath.Length > 0 Then
            txtFilename.Text = My.Settings.FilePath
        End If
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        If OpenFileDialog1.ShowDialog(Me) = DialogResult.OK Then
            txtAssembly.Text = OpenFileDialog1.FileName
        End If
    End Sub

    Private Sub btnBrowse2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse2.Click
        If OpenFileDialog1.ShowDialog(Me) = DialogResult.OK Then
            txtFilename.Text = OpenFileDialog1.FileName
            btnDisplayInputFile.Enabled = True
        End If
    End Sub

    Private Sub btnExecute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExecute.Click
        Windows.Forms.Cursor.Current = Cursors.WaitCursor
        Try
            ' Check that assembly is given
            If txtAssembly.Text.Trim.Length = 0 Then
                MessageBox.Show("Assembly must be given", "Exception", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)
                Exit Sub
            End If

            ' Reset result message
            txtResult.Text = ""

            ' Try to load assembly
            Dim objAssembly As Assembly = Assembly.LoadFrom(txtAssembly.Text)
            Dim strResult As New StringBuilder(String.Format("{0} Executing assembly: {1}", Date.Now.ToString(strTIMESTAMP_FORMAT), objAssembly.GetName.Name()))
            strResult.AppendLine("")
            strResult.AppendLine("")

            Dim objInstance As Object = Nothing
            Try
                ' Find class
                Dim objType As Type = Nothing
                For Each objTempType As Type In objAssembly.GetTypes()
                    If Not objTempType.BaseType Is Nothing _
                    AndAlso objTempType.BaseType.ToString() = strBASECLASS Then
                        objType = objTempType
                        Exit For
                    End If
                Next

                If objType Is Nothing Then
                    MessageBox.Show(String.Format("Class that inherits ""{0}"" is missing!", strBASECLASS), "Exception", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)
                    Exit Sub
                End If

                ' Create instance
                objInstance = Activator.CreateInstance(objType)

                ' Set Customer Id 
                If txtCustomerId.Text.Length > 0 Then objInstance.CustomerId = txtCustomerId.Text

                ' Set file name
                If txtFilename.Text.Length > 0 Then objInstance.SetInputFile(txtFilename.Text)

                ' Execute method
                Select Case objInstance.ExecuteService()
                    Case 1
                        ' Manage informational messages
                        strResult.AppendLine("Information:")
                        strResult.AppendLine(objInstance.GetInformation)
                    Case 2
                        ' Manage warnings
                        strResult.AppendLine("Warning:")
                        strResult.AppendLine(objInstance.GetWarnings)
                    Case 3
                        ' Manage informational messages AND warnings
                        strResult.AppendLine("Warning:")
                        strResult.AppendLine(objInstance.GetWarnings)
                        strResult.AppendLine("Information:")
                        strResult.AppendLine(objInstance.GetInformation)
                    Case 4
                        ' Manage exceptions
                        strResult.AppendLine("Exception:")
                        strResult.AppendLine(objInstance.GetExceptions)
                    Case 5
                        ' Manage informational messages AND exceptions
                        strResult.AppendLine("Exception:")
                        strResult.AppendLine(objInstance.GetExceptions)
                        strResult.AppendLine("Information:")
                        strResult.AppendLine(objInstance.GetInformation)
                    Case 6
                        ' Manage warnings AND exceptions
                        strResult.AppendLine("Exception:")
                        strResult.AppendLine(objInstance.GetExceptions)
                        strResult.AppendLine("Warning:")
                        strResult.AppendLine(objInstance.GetWarnings)
                    Case 7
                        ' Manage informational messages AND warnings AND exceptions
                        strResult.AppendLine("Exception:")
                        strResult.AppendLine(objInstance.GetExceptions)
                        strResult.AppendLine("Warning:")
                        strResult.AppendLine(objInstance.GetWarnings)
                        strResult.AppendLine("Information:")
                        strResult.AppendLine(objInstance.GetInformation)
                    Case Else
                        ' Success - do nothing
                End Select

                ' Check if output file is given
                m_strOutputFile = objInstance.GetOutputFile()
                If m_strOutputFile.Length > 0 Then
                    strResult.AppendLine(String.Format("Output file '{0}'", m_strOutputFile))
                    strResult.AppendLine()
                    btnDisplayOutputFile.Enabled = True
                Else
                    btnDisplayOutputFile.Enabled = False
                End If
                btnExportResult.Enabled = True

                strResult.AppendLine(String.Format("{0} Done!", Date.Now.ToString(strTIMESTAMP_FORMAT)))
                txtResult.Text &= strResult.ToString()

            Catch ex As Exception
                txtResult.Text &= String.Format("{0} Failed!", Date.Now.ToString(strTIMESTAMP_FORMAT))

                MessageBox.Show(ex.ToString(), "Exception", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)
            Finally
                If Not objInstance Is Nothing Then objInstance.Dispose()
                objInstance = Nothing

                ' Store settings
                My.Settings.AssemblyPath = txtAssembly.Text
                My.Settings.CustomerId = txtCustomerId.Text
                My.Settings.FilePath = txtFilename.Text
                My.Settings.Save()
            End Try
        Catch ex As Exception
            MessageBox.Show(ex.ToString(), "Exception", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)

        Finally
            Windows.Forms.Cursor.Current = Cursors.Default
        End Try
    End Sub

    Private Sub btnDisplayInputFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDisplayInputFile.Click
        System.Diagnostics.Process.Start("notepad.exe", txtFilename.Text)
    End Sub

    Private Sub btnDisplayOutputFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDisplayOutputFile.Click
        If m_strOutputFile.Contains(".pdf") Then
            System.Diagnostics.Process.Start("AcroRd32.exe", m_strOutputFile)
        Else
            System.Diagnostics.Process.Start("notepad.exe", m_strOutputFile)
        End If
    End Sub

    Private Sub btnExportResult_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportResult.Click
        If SaveFileDialog1.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then
            Dim objWriter As StreamWriter = New StreamWriter(SaveFileDialog1.FileName, False, Encoding.UTF8)
            objWriter.AutoFlush = True
            objWriter.NewLine = vbCrLf
            For Each strLine As String In txtResult.Lines
                objWriter.Write(strLine)
                objWriter.Write(vbCrLf)
            Next
            objWriter.Close()
        End If
    End Sub
End Class
